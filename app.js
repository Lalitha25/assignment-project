const express = require('express')
const app = express()
const port = 9000

app.get('/', (req, res) => {
    res.send('This is assingment for CICD Pipeline')
})

app.listen(port, () => {
    console.log('Application is listening on http://localhost:${port}')
})